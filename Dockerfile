FROM linuxserver/code-server:latest
RUN \
	apt-get update -y && apt-get install wget curl htop software-properties-common -y \
	&& wget -q https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O /tmp/packages-microsoft-prod.deb \
	&& dpkg -i /tmp/packages-microsoft-prod.deb \
	&& apt-get update -y \
	&& add-apt-repository universe \
	&& apt-get install -y powershell \
	&& apt-get clean -y \
	&& rm -rf /var/lib/apt/lists/*
